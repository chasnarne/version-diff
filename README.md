# PRI Apps Test Vs Prod version outputs

## Git clone, run (below commands) and open `index.html` file on a brower. 

```
 npm i
 npm start
 ```

## App list that will be output

* audits-v1, 
* qml-v1
* company-relationships-v1
* qualitysystem-v1
* financials-v1
* auditlinks-v1
* prifile-v1
* company-v1
* pri-audit-validator-v1
* configuration-v1
* priauditors-v1
* prichecklists-v1
* prigeneral-v1
* selfaudit-v1
* survey-v11
* advisory-v1
* user-profile-manager-service-v1
* change-tracking-v1
* prinetsuite-v1
* commodity-v1
* priemailapi-v1
* auditncr-v1
* pri-certificate-v1 
* agreement-v1
* auditcost-v1
* communications-v1
* pri-resources-v1
* certificate-v1
* pritgballot-v1
* survey-control-v1
* userdetail-v2
