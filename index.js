const fetch = require('node-fetch');
var fs = require('fs');
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0
let testUrl = "https://api-awstst.sae.org/api/services";
let prodUrl = "https://api2.sae.org/api/services";
let settings = { method: "Get" };
let services = ['audits-v1', 'qml-v1', 'company-relationships-v1', 'qualitysystem-v1', 'financials-v1', 'auditlinks-v1', 'prifile-v1', 'company-v1', 'pri-audit-validator-v1', 'configuration-v1', 'priauditors-v1', 'prichecklists-v1'
    , 'prigeneral-v1', 'selfaudit-v1', 'survey-v11', 'advisory-v1', 'user-profile-manager-service-v1', 'change-tracking-v1', 'prinetsuite-v1', 'commodity-v1', 'priemailapi-v1', 'auditncr-v1', 'pri-certificate-v1', 'agreement-v1',
'auditcost-v1', 'communications-v1', 'pri-resources-v1', 'certificate-v1', 'pritgballot-v1', 'survey-control-v1', 'userdetail-v2']
let versions = []
let dict = []
fetch(testUrl, settings)
    .then(res => res.json())
    .then((json) => {
        let test = JSON.parse(JSON.stringify(json))

        for(let i = 0; i< test.length; i++) {
            if(services.includes(test[i].name)) {
                dict.service = test[i].name
                let serviceInstances = test[i].instances
                var serviceInstance1 = ""
                for(let k = 0; k< serviceInstances.length; k++) {

                    if(k === 0) {
                        serviceInstance1 = serviceInstances[k].metaData["service.version"]
                    }
                    if(serviceInstance1 != serviceInstances[k].metaData["service.version"] )
                        serviceInstance1 = serviceInstance1 + serviceInstances[k].metaData["service.version"]
                }
                dict.push( {
                    "service":  test[i].name,
                    "testVersion" : serviceInstance1,
                    "prodVersion": ""

                })
            }
        }
        fetchProd()
    }

);

function fetchProd() {
    fetch(prodUrl, settings)
        .then(res => res.json())
        .then((json) => {
            let test = JSON.parse(JSON.stringify(json))

            for(let i = 0; i< test.length; i++) {
                if(services.includes(test[i].name)) {
                    dict.service = test[i].name
                    let serviceInstances = test[i].instances
                    var serviceInstance1 = ""
                    for(let k = 0; k< serviceInstances.length; k++) {

                        if(k === 0) {
                            serviceInstance1 = serviceInstances[k].metaData["service.version"]
                        }
                        if(serviceInstance1 != serviceInstances[k].metaData["service.version"] )
                            serviceInstance1 = serviceInstance1 + serviceInstances[k].metaData["service.version"]

                    }
                    const result = dict.find(({ service }) => service === test[i].name);
                    result.prodVersion = serviceInstance1
                }
            }

            function buildHtml(myObj) {
                var text = "<table><tr>" +
                    " <th>service</th>" +
                    "    <th>Test</th>" +
                    "    <th>Prod</th>" +
                    "</tr>"
                for (let x in myObj) {
                    if(myObj[x].service === undefined) {
                        console.log("undefined service")
                    }
                    else {
                        text += "<tr>" +
                            "<td>" + myObj[x].service + "</td>"
                            + "<td>" + myObj[x].testVersion + "</td>"
                            + "<td>" + myObj[x].prodVersion + "</td>"
                            + "</tr>";
                    }
                }
                text += "</table>"
                // concatenate header string
                // concatenate body string

                return '<!DOCTYPE html>'
                    + '<html><head></head><body>' + text + '</body></html>';
            };
            //console.log("Dicts Final " +JSON.stringify(dict));
            console.log(JSON.stringify(dict, null, 4))
            var fileName = 'index.html';
            var stream = fs.createWriteStream(fileName);

            stream.once('open', function(fd) {
                var html = buildHtml(dict);

                stream.end(html);
            });
        })
}


